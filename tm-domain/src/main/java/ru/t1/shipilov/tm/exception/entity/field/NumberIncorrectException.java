package ru.t1.shipilov.tm.exception.entity.field;

import org.jetbrains.annotations.NotNull;

public class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(@NotNull final String value, @NotNull Throwable cause) {
        super("Error! This value \"" + value + "\" is incorrect...");
    }

}
