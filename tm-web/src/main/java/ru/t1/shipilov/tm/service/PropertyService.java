package ru.t1.shipilov.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService {

    @Value("#{environment['database.url']}")
    private String dBUrl;

    @Value("#{environment['database.password']}")
    private String dBPassword;

    @Value("#{environment['database.username']}")
    private String dBUser;

    @Value("#{environment['database.schema']}")
    private String dBSchema;

    @Value("#{environment['database.driver']}")
    private String dBDriver;

    @Value("#{environment['database.l2Cache']}")
    private String dBL2Cache;

    @Value("#{environment['database.dialect']}")
    private String dBDialect;

    @Value("#{environment['database.showSQL']}")
    private String dBShowSQL;

    @Value("#{environment['database.hbm2ddl']}")
    private String dBHbm2DDL;

    @Value("#{environment['database.cacheRegionClass']}")
    private String dBCacheRegion;

    @Value("#{environment['database.useQueryCache']}")
    private String dBQueryCache;

    @Value("#{environment['database.useMinPuts']}")
    private String dBMinimalPuts;

    @Value("#{environment['database.regionPrefix']}")
    private String dBCacheRegionPrefix;

    @Value("#{environment['database.configFilePath']}")
    private String dBCacheProvider;

}
