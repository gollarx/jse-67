package ru.t1.shipilov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.shipilov.tm.api.service.ITaskService;
import ru.t1.shipilov.tm.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService
public class TaskCollectionEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Nullable
    @WebMethod(operationName = "findAll")
    @GetMapping()
    public List<TaskDTO> get() {
        return taskService.findAll();
    }

    @WebMethod(operationName = "saveCollection")
    @PostMapping
    public void post(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull @RequestBody List<TaskDTO> tasks) {
        taskService.saveAll(tasks);
    }

    @WebMethod(operationName = "updateCollection")
    @PutMapping
    public void put(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull @RequestBody List<TaskDTO> tasks) {
        taskService.saveAll(tasks);
    }

    @WebMethod(operationName = "deleteCollection")
    @DeleteMapping()
    public void delete(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull @RequestBody List<TaskDTO> tasks) {
        taskService.removeAll(tasks);
    }

}
