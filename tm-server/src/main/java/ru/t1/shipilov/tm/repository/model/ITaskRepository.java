package ru.t1.shipilov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.shipilov.tm.model.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
